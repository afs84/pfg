using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine; 

public static class XmlLoader
{
	
	public static T LoadXml<T>(string textAssetName) where T : class{
		TextAsset xmlTextAsset = (TextAsset) Resources.Load (textAssetName, typeof(TextAsset));

		using(var stream = new StringReader(xmlTextAsset.text)){
			
			var s = new XmlSerializer(typeof(T));
			T deserializedXml = s.Deserialize(stream) as T;
			return deserializedXml;
		}
	}

}

﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("Levels")]
public class Levels
{
	[XmlElement ("Developer")]
	public Developer developer;
	public class Developer
	{
		[XmlAttribute ("StartLevel")]
		public string startLevel;
	}
	
	[XmlElement ("Level")]
	public Level[] levels;
	public class Level
	{

		[XmlElement("Item")]
		public Item[] items;
	}
	
	public class Item
	{
		[XmlAttribute ("prefab")]
		public string prefab;
		
		[XmlAttribute ("x")]
		public string x;
		
		[XmlAttribute ("y")]
		public string y;

		[XmlAttribute ("z")]
		public string z;
		
		[XmlAttribute ("rot_x")]
		public string rot_x;
		
		[XmlAttribute ("rot_y")]
		public string rot_y;

		[XmlAttribute ("rot_z")]
		public string rot_z;
		
		[XmlAttribute ("scale_x")]
		public string scale_x;
		
		[XmlAttribute ("scale_y")]
		public string scale_y;

		[XmlAttribute ("scale_z")]
		public string scale_z;

		[XmlAttribute ("name")]
		public string name;
	}
}


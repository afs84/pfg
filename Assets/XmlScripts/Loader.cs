using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UnityEngine;

public class Loader{

	private Levels levels;

	private const string prefabsFolder = "Prefabs/";
	
	struct ItemStruct
	{
		public GameObject prefab;
		public float x;
		public float y;
		public float z;
		public float rot_x;
		public float rot_y;
		public float rot_z;
		public float scale_x;
		public float scale_y;
		public float scale_z;
		public string name;
	}
	
	// Almacenar prefabs prefabDict
	Dictionary<string,GameObject> objectsPool;

	// almacenar los valores de los items
	List<ItemStruct> sceneItemsList;

	Transform parentOfXmlItems;
	
	public const string xmlItemsObjectName = "XmlItems";

	public void generateItems (){
		objectsPool = new Dictionary<string, GameObject>();
		sceneItemsList = new List<ItemStruct>();

		
		parentOfXmlItems = new GameObject(xmlItemsObjectName).transform;
		//cargar fiechero xml
		levels = XmlLoader.LoadXml<Levels>("Levels");

		//nivel por el que se comienza,de momento solo hay uno
		int startLevel = int.Parse (levels.developer.startLevel);

		Levels.Level currentLevel = levels.levels[startLevel-1];

		// recorremos los items de cada level
		foreach (Levels.Item deserializedItem in currentLevel.items){

			string prefabString = deserializedItem.prefab;
			
			// si no existe en el diccionario de prefabs
			if (!objectsPool.ContainsKey(prefabString))
			{
				// se carga el prefab
				GameObject prefabObject = Resources.Load (prefabsFolder + prefabString, typeof(GameObject)) as GameObject;
				// si  hay un error al cargar
				if (prefabObject == null)
				{
					Debug.LogError ("Prefab \"" + prefabString + "\" no existe.");
					continue;
				}
				
				// se almacena en el diccionario
				objectsPool.Add (prefabString, prefabObject);
			}
			
			//asignamos los valores al struct
			ItemStruct item;
			item.prefab = objectsPool[prefabString];
			item.x = ZeroIfNull(deserializedItem.x);
			item.y = ZeroIfNull(deserializedItem.y);
			item.z = ZeroIfNull(deserializedItem.z);
			item.rot_x = ZeroIfNull(deserializedItem.rot_x);
			item.rot_y = ZeroIfNull(deserializedItem.rot_y);
			item.rot_z = ZeroIfNull(deserializedItem.rot_z);
			item.scale_x = OneIfNull(deserializedItem.scale_x);
			item.scale_y = OneIfNull(deserializedItem.scale_y);
			item.scale_z = OneIfNull(deserializedItem.scale_z);
			item.name = deserializedItem.name;
			
			sceneItemsList.Add (item);
		}
		
		//Se instancian los items
		foreach (ItemStruct item in sceneItemsList){
			//se instancia el objeto 
			GameObject newGameObject = MonoBehaviour.Instantiate(item.prefab) as GameObject;
			// se asignan los valores de los atributos
			setPos3D (newGameObject, new Vector3(item.x, item.y, item.z));
			setRot3D (newGameObject, new Vector3(item.rot_x, item.rot_y, item.rot_z));
			newGameObject.transform.localScale = new Vector3 (item.scale_x, item.scale_y, item.scale_z);
			if(item.name!= null){
				newGameObject.GetComponent<ClassInfoScript>().setName(item.name);
			}
			// se asigna el padre 
			newGameObject.transform.parent = parentOfXmlItems;
		}
	}


	// si el valor es nulo devuelve 0
	float ZeroIfNull (string value) { 
		return value == null ? 0 : float.Parse(value);	
	}
	// si el valor es nulo devuelve 1
	float OneIfNull  (string value) { 
		return value == null ? 1 : float.Parse(value);	
	}

	void setPos3D(GameObject g, Vector3 pos){
		
		g.transform.position = new Vector3 (
			pos.x,
			pos.y,
			pos.z
		);
	}

	void setRot3D(GameObject g, Vector3 rot){

		Quaternion rotation = Quaternion.identity;
		rotation.eulerAngles = rot;
		g.transform.localRotation = rotation;
	}

}

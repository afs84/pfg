using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Boomlagoon.JSON;
using System.IO;
using System.Text;

public class ClassInfoScript : MonoBehaviour {

	private IEnumerator coroutine;
	//private string name;
	private bool showInfo = false;
	//esta variable es para controlar que solo seconecte uan vez al entrar en un collider
	private bool connected = false;
	private bool isClass = false;
	private bool isDependency = false;
	private bool isOffice= false;
	private bool isExtra = false;
	public GUIStyle myStyle;
	public GUIStyle labelStyle;
	enum Types{classroom, dependency, office, extra};
	//string field1,field2,field3,field4;
	Room c = new Room();
	Office o = new Office();
	Dependency d = new Dependency();
	ExtraCollider e = new ExtraCollider();
	public float mult = 1f;
	int n_mult =0;
	int big =0;
	private bool noConnection = false;



	public void OnGUI() {

			if (showInfo) {
				if(noConnection){
				GUI.Box (new Rect ((Screen.width * 0.5f - Screen.width * 0.1f), (Screen.height*0.5f - Screen.height*0.1f), Screen.width * 0.27f , Screen.height*0.12f), "ERROR");
				GUI.Label (new Rect ((Screen.width * 0.51f - Screen.width * 0.1f), (Screen.height*0.53f - Screen.height*0.1f), Screen.width * 0.25f , Screen.height*0.12f), "Se produjo un error de conexión,no se pudo obtener la información");
					
				}else{
					GUI.Box (new Rect (Screen.width * 0.02f, Screen.height * 0.02f, Screen.width * 0.17f*mult, Screen.height * 0.75f), "", myStyle);
				}
				Debug.LogError("sin conexion"+noConnection);
				if (isClass) {
					if(!connected){
						string url = "http://localhost/ws/get/clase/"+getName();
						Debug.Log(url);
						StartCoroutine(HandleWWWRequest(url, (www) => {
							Debug.Log("Start coroutine");
							WebserviceResponse(www.text, Types.classroom);
							connected = true;
						}));
					}


				if(!noConnection){
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(c.getName()) , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.75f), "Información:" , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(c.getInfo()) , labelStyle);
					//GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(c.getLink()) , labelStyle);
					if( GUI.Button(new Rect (Screen.width * 0.03f, Screen.height * 0.5f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(c.getLink()),labelStyle) )
					{
						//Application.OpenURL(setupURL(c.getLink()));
						Application.ExternalEval("window.open('"+setupURL(c.getLink())+"','Window title')");
					}
				}
					
					
				} else if (isDependency) {
					if(!connected){
						string url = "http://localhost/ws/get/dependencia/"+getName();
						StartCoroutine(HandleWWWRequest(url, (www) => {
							Debug.Log("Start coroutine");
							WebserviceResponse(www.text, Types.dependency);
							connected = true;
						}));
					}

					
				if(!noConnection){
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(d.getName()) , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.75f),"Horario:" , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(d.getSchedule()) , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.35f, Screen.width * 0.15f, Screen.height * 0.75f), "Información:" , labelStyle);
					GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.40f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(d.getInfo()), labelStyle);
					
					if( GUI.Button(new Rect (Screen.width * 0.03f, Screen.height * 0.65f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(d.getLink()),labelStyle) )
					{
						//Application.OpenURL(setupURL(d.getLink()));
						Application.ExternalEval("window.open('"+setupURL(d.getLink())+"','Window title')");
					}
				}
					
				}else if(isOffice){
					if(!connected){
						string url = "http://localhost/ws/get/despacho/"+getName();
						StartCoroutine(HandleWWWRequest(url, (www) => {
							Debug.Log("Start coroutine");
							WebserviceResponse(www.text, Types.office);
							connected = true;
						}));
					}
				if(!noConnection){
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(o.getName()) , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.75f), "Profesor:" , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(o.getProfesor()), labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.35f, Screen.width * 0.15f, Screen.height * 0.75f),"Tutoría:" , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.40f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(o.getTutorship()) , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.55f, Screen.width * 0.15f, Screen.height * 0.75f), "Información:" , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.60f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(o.getInfo()) , labelStyle);
						
						if( GUI.Button(new Rect (Screen.width * 0.03f, Screen.height * 0.70f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(o.getLink()),labelStyle) )
						{
							//Application.OpenURL(setupURL(o.getLink()));
							Application.ExternalEval("window.open('"+setupURL(o.getLink())+"','Window title')");
						}
					}
				}
				else if (isExtra) {
					Debug.Log("extra");
					if(!connected){
						string url = "http://localhost/ws/get/collider/"+getName();
						Debug.Log(url);
						StartCoroutine(HandleWWWRequest(url, (www) => {
							Debug.Log("Start coroutine");
							WebserviceResponse(www.text, Types.extra);
							connected = true;
						}));
					}
					
						
				if(!noConnection){
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.04f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(e.getName()) , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.15f, Screen.width * 0.15f, Screen.height * 0.75f), "Información:" , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.2f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(e.getInfo()) , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.45f, Screen.width * 0.15f, Screen.height * 0.75f), "Duracion:" , labelStyle);
						GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.50f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(e.getDuration()) , labelStyle);
						//GUI.Label (new Rect (Screen.width * 0.03f, Screen.height * 0.6f, Screen.width * 0.15f, Screen.height * 0.75f), putLine(c.getLink()) , labelStyle);
						if( GUI.Button(new Rect (Screen.width * 0.03f, Screen.height * 0.63f, Screen.width * 0.15f, Screen.height * 0.75f),putLine(e.getLink()),labelStyle) )
						{
							//Application.OpenURL(setupURL(e.getLink()));
							Application.ExternalEval("window.open('"+setupURL(e.getLink())+"','Window title')");
						}
					}
					
				} 
			}
		

	}
	//obtener el nombre
	public void setName(string value){
		name = value;
	}
	//cambiar el nombre
	public string getName(){
		return name;
	}
	//cambiar el  valor de showInfo
	public void changeInfoState(){
		showInfo = !showInfo;
	}
	public void changeClass(){
		isClass = !isClass;
	}
	public void changeDependency(){
		isDependency = !isDependency;
	}
	public void changeOffice(){
		isOffice = !isOffice;
	}
	public void changeExtra(){
		isExtra = !isExtra;
	}
	
	IEnumerator HandleWWWRequest(string url, System.Action<WWW> onSuccess) {
		WWW www = new WWW(url);
		yield return www;
		try{
			if (string.IsNullOrEmpty(www.error)) {
				onSuccess(www);
				noConnection = false;
			} else {
				Debug.LogWarning("WWW request returned an error.");
				noConnection = true;
			}
		}catch(Exception e){
			Debug.LogException(e,this);
		}
	}
	void WebserviceResponse(string jsonResponse, Types type){
		JSONObject jo = new JSONObject();

		try{
			jo = JSONObject.Parse (jsonResponse);

			switch (type) {
				case Types.classroom:
					//Debug.Log(field2);
					//field2 = jo.GetString ("info");
					c.setName(putSpace(jo.GetString ("nombre")));
					c.setInfo(jo.GetString ("info"));
					c.setLink(jo.GetString ("enlace").Replace(@"\",string.Empty));
					break;
				case Types.dependency:
					d.setName(putSpace(jo.GetString ("nombre")));
					d.setInfo(jo.GetString ("info"));
					d.setSchedule(jo.GetString ("horario"));
					d.setLink(jo.GetString ("enlace").Replace(@"\",string.Empty));
					break;
				case Types.office:
					o.setName(putSpace(jo.GetString ("nombre")));
					o.setInfo((jo.GetString ("info")));
					o.setProfesor(jo.GetString("profesor"));
					o.setTutorship(jo.GetString("tutorias"));
					o.setLink(jo.GetString ("enlace").Replace(@"\",string.Empty));
					break;
				case Types.extra:
					e.setName(putSpace(jo.GetString ("nombre")));
					e.setInfo(jo.GetString ("info"));
					e.setDuration(jo.GetString("duracion"));
					e.setLink(jo.GetString ("enlace").Replace(@"\",string.Empty));
					break;
				}
		}catch(System.Exception e){
			//Debug.LogException(e,this);
		}
	}
	/*string readPath(){
		string filepath = "wsPath.txt";
		string wspath=Environment.NewLine;

		if (File.Exists(filepath))
			 wspath = File.ReadAllText(filepath);

		return wspath;
	}*/
	//Controla el tamaño de las lineas 

	/*bool isVocal(int c){
		return c == 97 || c == 101 || c == 105 || c == 111 || c == 117 || c == 65 || c == 69 || c == 73 || c == 79 || c == 85;
	}*/

	int blankPosition(int i, string s){
		int idx = i;

		while (Convert.ToChar(s[i])!=' ' && i>0) {
			i--;
		}			
		if (i <= 0)
			return idx;
		else
			return i;
	}
	String putLine(String s){
		try{
			for (int i =0; i < s.Length; i++) {
				if(i>0 && i%14==0){
					int line = blankPosition(i,s);
					if (line == i)
						s = s.Insert(i,"\n");
					else{
						StringBuilder sb = new StringBuilder(s);
						sb[line] = '\n';
						s = sb.ToString();
					}
				}
			}
			//Debug.Log("cadena = "+s);
			//Debug.Log("mayor linea ="+bigger_line(s));

			/*int b_line = bigger_line(s);
			if(b_line > 14){
				if(n_mult ==0){
					mult = mult +(0.01f*(float)b_line);
					n_mult++;
				}
			}*/

			if(bigger_line(s)>big)
				big=bigger_line(s);
			//Debug.Log("La mayor linea es"+ big);

			if(big > 20){
				if(n_mult ==0){
					mult = mult +(0.02f*(float)big);
					n_mult++;
				}
			}

			return s;
		}catch(NullReferenceException e){
			Debug.Log("NullReference");
			return "";
		}
	}

	private int bigger_line(string s){
		int difference = 0;
		int start = 0;
		for (int i=0; i<s.Length; i++) {
			if(s[i]=='\n' || i ==(s.Length-1)){
				//Debug.Log(i+" es un espacio");
				int n_dif = i - start;
				if(n_dif > difference)
					difference = n_dif;
				start = i;
			}
		}
		if (start == 0)
			return 1;
		else
			return difference;
	}
	//cambio _ por  espacios
	String putSpace(String s){
		StringBuilder newString = new StringBuilder(s);
		newString.Replace ('_',' ');
		return newString.ToString();
	}
	//cambia el valor de la variable connected
	public void changeConnected(){
		connected  = !connected;
	}

	public string setupURL(string link){
		if (link.Length<7 || link.Substring (0,7) != "http://")
			return "http://" + link;
		else
			return link;
	}
}

public class Room{
	protected string name;
	protected string info;
	protected float x,y,z,scale_x,scale_y,scale_z,rot_x,rot_y,rot_z;
	protected string link;
    
	public Room (){}
	public Room (string n, string i,float x,float y, float z, float rot_x,float rot_y, float rot_z,float scale_x, float scale_y, float scale_z,string link){
		name = n;
		info = i;
		this.x = x;
		this.y = y;
		this.z = z;
		this.rot_x = rot_x;
		this.rot_y = rot_y;
		this.rot_z = rot_z;
		this.scale_x = scale_x;
		this.scale_y = scale_y;
		this.scale_z = scale_z;
		this.link = link;
	}

	public void setName(string n){
		name = n;
	}
	public void setInfo(string i){
		info = i;
	}
	public string getName(){
		return name;
	}

	public string getInfo(){
		return info;
	}
	public void setX(float x){
		this.x = x;
	}
	
	public float  getX(){
		return x;
	}
	
	public void setY(float y){
		this.y = y;
	}
	
	public float getY(){
		return y;
	}
	public void setZ(float z){
		this.z = z;
	}
	
	public float  getZ(){
		return z;
	}
	
	public void set_scaleX(float x){
		this.scale_x = x;
	}
	
	public float  get_scaleX(){
		return scale_x;
	}
	
	public void set_scaleY(float y){
		this.scale_y = y;
	}
	
	public float get_scaleY(){
		return scale_y;
	}
	public void set_scaleZ(float z){
		this.scale_z = z;
	}
	
	public float  get_scaleZ(){
		return scale_z;
	}
	
	public void set_rotX(float x){
		rot_x = x;
	}
	
	public float  get_rotX(){
		return rot_x;
	}
	
	public void set_rotY(float y){
		this.rot_y = y;
	}
	
	public float get_rotY(){
		return rot_y;
	}
	public void set_rotZ(float z){
		this.rot_z = z;
	}
	
	public float  get_rotZ(){
		return rot_z;
	}
	public void setLink(string l){
		link = l;
	}
	public string getLink(){
		return link;
	}
}
public class Office:Room{
	private string profesor;
	private string tutorship;

	public Office(){
	}

	public Office (string n, string i, string p, string t, float x,float y, float z, float rot_x,float rot_y, float rot_z,float scale_x, float scale_y, float scale_z, string link){
		name = n;
		info = i; 
		profesor = p;
		tutorship = t;
		this.x = x;
		this.y = y;
		this.z = z;
		this.rot_x = rot_x;
		this.rot_y = rot_y;
		this.rot_z = rot_z;
		this.scale_x = scale_x;
		this.scale_y = scale_y;
		this.scale_z = scale_z;
		this.link = link;
	}

	public void setProfesor(string p){
		profesor = p;
	}
	public void setTutorship(string t){
		tutorship = t;
	}
	public string getProfesor(){
		return profesor;
	}
	public string getTutorship(){
		return tutorship;
	}

}
public class Dependency:Room{
	private string schedule;

	public Dependency (string n, string i, string sch, float x,float y, float z, float rot_x,float rot_y, float rot_z,float scale_x, float scale_y, float scale_z, string link){
		name = n;
		info = i; 
		schedule = sch;
		this.x = x;
		this.y = y;
		this.z = z;
		this.rot_x = rot_x;
		this.rot_y = rot_y;
		this.rot_z = rot_z;
		this.scale_x = scale_x;
		this.scale_y = scale_y;
		this.scale_z = scale_z;
		this.link = link;
	}

	
	public Dependency(){
	}

	public void setSchedule(string s){
		schedule = s;
	}
	public string getSchedule(){
		return schedule;
	}	
}
public class ExtraCollider:Room{
	private string duration;

	public ExtraCollider(){}
	public ExtraCollider(string name,string info, string duration, float x,float y, float z, float rot_x,float rot_y, float rot_z,float scale_x, float scale_y, float scale_z, string link){
		this.name = name;
		this.info = info;
		this.duration = duration;
		this.x = x;
		this.y = y;
		this.z = z;
		this.rot_x = rot_x;
		this.rot_y = rot_y;
		this.rot_z = rot_z;
		this.scale_x = scale_x;
		this.scale_y = scale_y;
		this.scale_z = scale_z;
		this.link = link;
	}

	public void setDuration(string d){
		duration = d;
	}
	public string getDuration(){
		return duration;
	}
	
}
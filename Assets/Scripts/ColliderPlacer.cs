﻿using UnityEngine;
using System.Collections;
using Boomlagoon.JSON;
using System.Globalization;

public class ColliderPlacer : MonoBehaviour{

	string colliders;


	public void place (JSONArray array,string type){
		for(int i=0; i<array.Length; i++){
			JSONObject jo2 = new JSONObject();
			jo2 = JSONObject.Parse(array[i].ToString());
			Debug.LogError(JSONObject.Parse(array[i].ToString()));
			
			//rellenar los valores detransform con los valores del json
			GameObject go= Instantiate (Resources.Load("Prefabs/"+type))as GameObject;
			//go.GetComponent<ClassInfoScript> ().setName(jo2.GetString("nombre"));
			ExtraCollider extra= new ExtraCollider(jo2.GetString("nombre"), jo2.GetString("info")," ", float.Parse(jo2.GetString("x"), CultureInfo.InvariantCulture.NumberFormat),
			                                       float.Parse(jo2.GetString("y"), CultureInfo.InvariantCulture.NumberFormat),float.Parse(jo2.GetString("z"), CultureInfo.InvariantCulture.NumberFormat),
			                                       float.Parse(jo2.GetString("rotacion_x"), CultureInfo.InvariantCulture.NumberFormat),float.Parse(jo2.GetString("rotacion_y"), CultureInfo.InvariantCulture.NumberFormat), float.Parse(jo2.GetString("rotacion_z"), CultureInfo.InvariantCulture.NumberFormat),
			                                       float.Parse(jo2.GetString("escala_x"), CultureInfo.InvariantCulture.NumberFormat),float.Parse(jo2.GetString("escala_y"), CultureInfo.InvariantCulture.NumberFormat), float.Parse(jo2.GetString("escala_z"), CultureInfo.InvariantCulture.NumberFormat),jo2.GetString("enlace"));
			
			go.transform.position = new Vector3 (extra.getX(),extra.getY(),extra.getZ ());
			go.transform.rotation = Quaternion.Euler (extra.get_rotX(),extra.get_rotY(),extra.get_rotZ());
			go.transform.localScale=new Vector3 (extra.get_scaleX(),extra.get_scaleY(),extra.get_scaleZ());
			go.GetComponent<ClassInfoScript>().setName(extra.getName());
		}
	}

}

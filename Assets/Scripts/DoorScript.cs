﻿using UnityEngine;
using System.Collections;
using System;

public class DoorScript : MonoBehaviour {

	public Animator anim;
	bool opened =false;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();

	}
	
	void OnMouseDown() {
		Debug.Log ("Mouse clicked");
		opened = !opened;
		anim.SetBool ("open",opened);
	}

}

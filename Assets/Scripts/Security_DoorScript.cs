using UnityEngine;
using System.Collections;

public class Security_DoorScript : DoorScript {

	// Use this for initialization
	Vector3 myScale;
	bool openedLeft = false;
	bool openedRight = false;

	void Start () {
		myScale = transform.localScale;
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void OnMouseDown() {
		Debug.Log ("Mouse clicked");
		Debug.Log (myScale.x);

		if (myScale.x < 0 ) {
			Debug.Log ("Right Door");
			openedRight=!openedRight;
			anim.SetBool ("openR",openedRight);
		}else {

			Debug.Log ("Left Door");
			openedLeft = !openedLeft;
			anim.SetBool ("openL",openedLeft);
		}

	}
}

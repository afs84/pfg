using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;
using System;
using UnityEngine;
using System.Collections;
using System.Net;
using System.IO;
using System;
using Boomlagoon.JSON;
using System.Globalization;
using System.Collections.Generic;

public class InitializerScript : MonoBehaviour {
	
	Loader loader;
	bool failLoading = false;
	bool showError = false;
	
	// Use this for initialization
	void Start () {
		Debug.Log ("Cargando");
		loader = new Loader ();
		loader.generateItems ();
		loadColliders("collider","extra");
		loadColliders ("clase","ClassInfoCollider");
		loadColliders ("dependencia","SecretaryCollider");
		loadColliders ("despacho","Office1Collider");
		//loadLight ();
		
		GameObject up = Instantiate (Resources.Load("Prefabs/UpstairPlane1"))as GameObject;
		up.transform.position = new Vector3 (10.576f,-0.234f,7.56f);
		
		
	}
	void OnGUI(){
		if (showError) {
			GUI.Box (new Rect ((Screen.width * 0.5f - Screen.width * 0.1f), (Screen.height*0.5f - Screen.height*0.1f), Screen.width * 0.27f , Screen.height*0.22f), "ERROR");
			GUI.Label (new Rect ((Screen.width * 0.51f - Screen.width * 0.1f), (Screen.height*0.53f - Screen.height*0.1f), Screen.width * 0.25f , Screen.height*0.12f), "Se produjo un error de conexión,no se pudo obtener la información¿Desea recargar la página?");
			if (GUI.Button (new Rect (Screen.width *0.5f, Screen.height*0.52f , Screen.width * 0.07f , Screen.height*0.08f), "Recargar"))
				Application.ExternalEval("document.location.reload(true)");
		}
	}
	
	
	void loadColliders(String table,string type){
		
		string url = "http://localhost/ws/getAll/"+table;
		Debug.Log(url);
		StartCoroutine(wsRequest(url, (www) => {
			Debug.Log("Start coroutine");
			getColliders(www.text,type);
		}));
	}
	
	IEnumerator wsRequest(string url, System.Action<WWW> onSuccess) {
		WWW www = new WWW(url);
		yield return www;
		try{
			if (string.IsNullOrEmpty(www.error)) {
				onSuccess(www);
			} else {
				Debug.LogWarning("WWW request returned an error.");
				Debug.Log(www.error);
				showError = true;
			}
		}catch(Exception e){
			Debug.LogException(e,this);
		}
	}
	
	void getColliders(string jsonResponse,string type){
		//Debug.Log(jsonResponse);
		JSONObject jo = new JSONObject();
		try{
			jo = JSONObject.Parse (jsonResponse);
			JSONArray array = jo.GetArray("Colliders");
			ColliderPlacer cp = new ColliderPlacer ();
			cp.place(array,type);

		}catch(System.Exception e){
			Debug.LogException(e,this);
		}
	}
	
	private void loadSinkColliders(){
		GameObject go1 = Instantiate (Resources.Load("Prefabs/SinkCollider"))as GameObject;
		go1.transform.position = new Vector3 (-48f,0.6f,-3f);
		
		
	}
	
	/*private void loadLight(){
		float x = 13.853f;
		float y = 1.583f;
		float z = -8.28f;
		 
		for(int i=0;i<32;i++){
			GameObject go1 = Instantiate (Resources.Load("Prefabs/PointLight"))as GameObject;
			go1.transform.position = new Vector3 (x,y,z);
			x -= 2.548f; 
		}
	}*/
}

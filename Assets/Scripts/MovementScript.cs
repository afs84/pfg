﻿using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour {
	float speed = 7f;
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < 27f) {
			if (Input.GetKey (KeyCode.D)) {
				Debug.Log ("Right");
				transform.Translate (Vector3.right * speed * Time.deltaTime);
			}
		}

		if (transform.position.x > -103f) {
			if (Input.GetKey (KeyCode.A)) {
				Debug.Log ("Left");
				transform.Translate (Vector3.left * speed * Time.deltaTime);
			}
		}

		if (Input.GetKey (KeyCode.W)) {
			if (transform.position.z < 47.2f){
				Debug.Log("Forward");
				transform.Translate(Vector3.forward*speed*Time.deltaTime);
			}
		}
		if (transform.position.z > -18.2f) {
			if (Input.GetKey (KeyCode.S)) {
				Debug.Log ("Back");
				transform.Translate (Vector3.back * speed * Time.deltaTime);
			}
		}

		if (transform.position.y < 19f) {
			if (Input.GetKey (KeyCode.UpArrow)) {
				Debug.Log ("Up");
				transform.Translate (Vector3.up * speed * Time.deltaTime);
			}
		}

		if (transform.position.y > -5f) {
			if (Input.GetKey (KeyCode.DownArrow)) {
				Debug.Log ("Down");
				transform.Translate (Vector3.down * speed * Time.deltaTime);
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CollisionScript : MonoBehaviour {
	bool [] loadedChairs;
	List<KeyValuePair<float,float>> chairList = new List<KeyValuePair<float,float>>();

	void Start(){
		/*for (int i=0; i<20; i++) {
			loadedChairs[i]=false;
		}*/
		populateList ();
		loadedChairs = new bool[chairList.Count];
		//setLights ();
	}
	void Update() {
		//Debug.Log ("distance = "+distance(chairList[16].Key, chairList[16].Value));
		int idx = nearest(chairList);
		//Debug.Log ("El mas cercano es "+idx);
		loadChairs (idx);

	}

	public void OnTriggerEnter(Collider other) {
		Debug.Log ("Entering trigger");
		//obtenemos el gameobject al que pertenece el trigger en el que hemos entrado
		GameObject obj = other.gameObject;
		//cambiamos el valor de showInfo ,si es el caso
		changingState(obj);

		/*if (other.gameObject.tag == "sink") {
			if(other.gameObject.GetComponent<SinkScript>().count==0){
				GameObject go = Instantiate (Resources.Load("Prefabs/Sink"))as GameObject;
				go.transform.position = new Vector3(other.gameObject.GetComponent<SinkScript>().x,other.gameObject.GetComponent<SinkScript>().y,other.gameObject.GetComponent<SinkScript>().z);
				other.gameObject.GetComponent<SinkScript>().count++;
			}
		}*/
	}

	public void OnTriggerExit(Collider other){
		Debug.Log ("Exit trigger");
		//obtenemos el gameobject al que pertenece el trigger en el que hemos entrado
		GameObject obj = other.gameObject;
		//cambiamos el valor de showInfo ,si es el caso
		changingState(obj);
		try{
			obj.GetComponent<ClassInfoScript> ().changeConnected ();
		}catch(NullReferenceException e){
		}

	}

	public void changingState(GameObject obj){
		Debug.Log (obj.tag);
		if (obj.tag == "DependencyInfo" || obj.tag == "ClassInfo" || obj.tag == "OfficeInfo"|| obj.tag == "ExtraInfo") {
			obj.GetComponent<ClassInfoScript>().changeInfoState();

			switch(obj.tag){
			case "ClassInfo":
				Debug.Log("Class");
				obj.GetComponent<ClassInfoScript>().changeClass();		
				break;
			case "DependencyInfo":
				Debug.Log("Dependency");
				obj.GetComponent<ClassInfoScript>().changeDependency();		
				break;
			case "OfficeInfo":
				Debug.Log("Office");
				obj.GetComponent<ClassInfoScript>().changeOffice();		
				break;
			case "ExtraInfo":
				Debug.Log("Extra");
				obj.GetComponent<ClassInfoScript>().changeExtra();		
				break;
			}
		}
	}
	
	void populateList(){
		int idx = 0;
		float [] xs = {-44.51f,-30.43f,-16.35f,-2.27f};
		float [] zs = {21.38f,26.12f,35.55f,30.77f,40.21f,12.01f};
		
		for(int i=0;i < 4;i++){
			for(int j=0;j < 6;j++){
				KeyValuePair<float, float> k = new KeyValuePair<float, float>(xs[i],zs[j]);
				chairList.Add(k);
			}
		}
		//clases grandes
		chairList.Add(new KeyValuePair<float, float>(-30.52f,-2.16f));
		chairList.Add(new KeyValuePair<float, float>(-30.52f,5.01f));
		chairList.Add(new KeyValuePair<float, float>(-44.58f,-2.16f));
		chairList.Add(new KeyValuePair<float, float>(-44.58f, 4.94f));
		//otras
		chairList.Add(new KeyValuePair<float, float>(-44.51f,12.02f));
		chairList.Add(new KeyValuePair<float, float>(-30.43f,12.02f));
		//chairList.Add(new KeyValuePair<float, float>(-16.35f,-12.02f));
		
		chairList.Add(new KeyValuePair<float, float>(-16.35f,7.31f));
		chairList.Add(new KeyValuePair<float, float>(-16.35f,2.6f));
		chairList.Add(new KeyValuePair<float, float>(-16.35f,-2.11f));
		chairList.Add(new KeyValuePair<float, float>(-2.27f,7.31f));
		chairList.Add(new KeyValuePair<float, float>(-2.27f,2.6f));
		chairList.Add(new KeyValuePair<float, float>(-2.27f,-2.11f));


		/*foreach (KeyValuePair<float,float> l in chairList) {
			Debug.Log(l.Key+" "+l.Value);
			GameObject go= Instantiate (Resources.Load("Prefabs/proximitySensor"))as GameObject;
			go.transform.position = new Vector3(l.Key,0f,l.Value+2);
			go.GetComponent<beaconScript>().setIdx(idx);
			idx++;
		}*/
	}

	double distance(float x2,float y2){
		return Math.Sqrt (Math.Pow((x2-transform.position.x),2)+Math.Pow((y2-transform.position.z),2));
	}

	int nearest(List<KeyValuePair<float,float>> list){
		double d = 10000;
		int idx = 0;
		int nearest = 0;

		foreach (KeyValuePair<float,float> l in list) {
			//Debug.Log(l.Key+" "+l.Value );
			//Debug.Log(distance(l.Key,l.Value)+" "+idx);

			if(distance(l.Key,l.Value)< d){
				d=distance(l.Key,l.Value);
				nearest = idx;
			}
			idx++;
		}

		return nearest;
	}

	void loadChairs(int idx){
		try{
			if (!loadedChairs [idx]) {
				GameObject go;
				if(idx==24||idx==25||idx==26||idx==27)
					go = Instantiate (Resources.Load("Prefabs/BigClassChairs"))as GameObject;
				else 
					go = Instantiate (Resources.Load("Prefabs/ClassChairs"))as GameObject;

				go.transform.position = new Vector3(chairList[idx].Key,0f,chairList[idx].Value);
				go.transform.Rotate(270,0,0);
				loadedChairs[idx] = true;
			}
		}catch(Exception e){

		}
	}

	void setLights(){
		//lamparas pasillo principal
		Vector3 pos = new Vector3 (13.96f,1.57f,-8.02f);

		for (int i =0; i<32; i++) {
			GameObject go = Instantiate (Resources.Load("Prefabs/PointLight"))as GameObject;
			go.transform.position = pos;
			pos.x += -2.88f;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class SizeScript : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		setSize (other);
	}
	void OnTriggerExit(Collider other) {
		setSize (other);
	}
	void setSize(Collider other){
		if (other.gameObject.transform.localScale.y != 0.00002f) {
			other.gameObject.transform.localScale = new Vector3(other.gameObject.transform.localScale.x,0.00002f,other.gameObject.transform.localScale.z);
		}else if(other.gameObject.transform.localScale.y == 0.00002f){
			other.gameObject.transform.localScale = new Vector3(other.gameObject.transform.localScale.x,0.45f,other.gameObject.transform.localScale.z);
		}
	}
}

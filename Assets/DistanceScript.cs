﻿using UnityEngine;
using System.Collections;
using System;

public class DistanceScript : MonoBehaviour {
	bool eChairs = false;
	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		GameObject chairs=null;
		if (distance_from (GameObject.Find ("E_Beacon")) < 1) {
			if(!eChairs){
				chairs = Instantiate (Resources.Load ("Prefabs/BigClassChairs"))as GameObject;
				chairs.transform.position = new Vector3 (-44.58f, 0f, -2.16f);
				chairs.transform.Rotate (270, 0, 0);
				eChairs = true;
			}

		} else {
			Destroy(chairs);
			eChairs =false;
		}

	}

	double distance_from(GameObject beacon){
		double distance = Math.Sqrt((beacon.transform.position.x - transform.position.x)+(beacon.transform.position.y - transform.position.y));
		//Debug.Log ("distancia = "+distance);
		return distance;
	}
}
